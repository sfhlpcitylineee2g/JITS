package com.jits.shipping.util;

public enum DistributionCenter {
	AL(DistributionCity.DC2), 
	AK(DistributionCity.DC3), 
	AZ(DistributionCity.DC3),
	AR(DistributionCity.DC2),
	CA(DistributionCity.DC3),
	CO(DistributionCity.DC3), 
	CT(DistributionCity.DC1), 
	DE(DistributionCity.DC1),
	DC(DistributionCity.DC1),
	FL(DistributionCity.DC1),
	GA(DistributionCity.DC1), 
	HI(DistributionCity.NA), 
	ID(DistributionCity.DC3),
	IL(DistributionCity.DC2),
	IN(DistributionCity.DC2),
	IA(DistributionCity.DC2), 
	KS(DistributionCity.DC3), 
	KY(DistributionCity.DC2),
	LA(DistributionCity.DC2),
	ME(DistributionCity.DC1),
	MD(DistributionCity.DC1), 
	MA(DistributionCity.DC1), 
	MI(DistributionCity.DC2),
	MN(DistributionCity.DC2),
	MS(DistributionCity.DC2),
	MO(DistributionCity.DC2),
	MT(DistributionCity.DC3),
	NE(DistributionCity.DC3),
	NV(DistributionCity.DC3),
	NH(DistributionCity.DC1),
	NJ(DistributionCity.DC1),
	NM(DistributionCity.DC3),
	NY(DistributionCity.DC1),
	NC(DistributionCity.DC1),
	ND(DistributionCity.DC3),
	OH(DistributionCity.DC1),
	OK(DistributionCity.DC3),
	OR(DistributionCity.DC3),
	PA(DistributionCity.DC1),
	RI(DistributionCity.DC1),
	SC(DistributionCity.DC1),
	SD(DistributionCity.DC3),
	TN(DistributionCity.DC2),
	TX(DistributionCity.DC3),
	UT(DistributionCity.DC3),
	VT(DistributionCity.DC1),
	VA(DistributionCity.DC1),
	WA(DistributionCity.DC2),
	WV(DistributionCity.DC1),
	WI(DistributionCity.DC2),
	WY(DistributionCity.DC3);
	
	DistributionCity distributionCity;
	
	DistributionCenter(DistributionCity distributionCity){
		this.distributionCity = distributionCity;
	}
	
	public DistributionCity getDistributionCenter(){
		return distributionCity;
	}
}
