package com.jits.shipping.util;

public enum DistributionCity {
	DC1("Raleigh"),
	DC2("Kansas City"),
	DC3("Denver"),
	NA("Not Applicable");
	
	String city;
	
	DistributionCity(String city){
		this.city = city;
	}
	
	public String getCity(){
		return city;
	}
	
	
}
