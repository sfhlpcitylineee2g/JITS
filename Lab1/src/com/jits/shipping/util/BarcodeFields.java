package com.jits.shipping.util;

public class BarcodeFields {
	public static final int ID = 0;
	public static final int SHIPMETHOD = 1;
	public static final int FROMZIP = 2;
	public static final int TOZIP = 3;
	public static final int WEIGHT = 4;
	public static final int HEIGHT = 5;
	public static final int WIDTH = 6;
	public static final int DEPTH = 7;
	public static final int OTHER = 8;
	public static final int HAZARDS = 9;
	public static final int ADDRESS = 10;
}
