package com.jits.shipping.util;

public enum ShipMethod {
	GRD("Ground"), AIR("Air"), RAL("Railroad");
	
	String shippingType;
	
	ShipMethod(String shippingType){
		this.shippingType = shippingType;
	}
	
	public String getShippingType(){
		return shippingType;
	}
}
