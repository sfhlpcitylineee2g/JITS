package com.jits.shipping.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;

public class ValidateZipCode {
	private static final Logger LOG = LoggerFactory.getLogger("ValidateZipCode");
	
	public static void validate(String zipcode, String toOrFrom) throws InvalidBarcodeDataException{
		if (zipcode.contains("[a-zA-Z]+") || zipcode.length() > 5) {
			LOG.error("Zipcode was"+zipcode+". Zipcodes can only be comprised of 5 digits");
			throw new InvalidBarcodeDataException(toOrFrom+" Zipcode falls outside of accepted range");
		}
	}
}
