package com.jits.shipping.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jits.shipping.Dimensions;
import com.jits.shipping.exceptions.InvalidBarcodeDataException;

public class ValidateDimensions {
	private static final Logger LOG = LoggerFactory.getLogger("ValidateDimensions");
	
	public static void validate(Dimensions dimensions) throws InvalidBarcodeDataException{
		if (dimensions.getHeight() < 0){
			LOG.error("Height was "+dimensions.getHeight()+". Values must be greater than 0");
			throw new InvalidBarcodeDataException("Height fall outside of accepted range");
		}
		if (dimensions.getWidth() < 0){
			LOG.error("Width was "+dimensions.getWidth()+". Values must be greater than 0");
			throw new InvalidBarcodeDataException("Width fall outside of accepted range");
		}
		if (dimensions.getDepth() < 0){
			LOG.error("Depth was "+dimensions.getDepth()+". Values must be greater than 0");
			throw new InvalidBarcodeDataException("Depth fall outside of accepted range");
		}
	}
}
