package com.jits.shipping.exceptions;

import java.util.ArrayList;

public class InvalidBarcodeDataException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidBarcodeDataException() {
	}

	public InvalidBarcodeDataException(String arg0) {
		super(arg0);
	}

	public InvalidBarcodeDataException(String string, ArrayList<Exception> exceptionHistory) {

	}
}
