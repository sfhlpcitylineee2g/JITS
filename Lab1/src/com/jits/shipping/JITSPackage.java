package com.jits.shipping;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.parse.BarcodeParse;
import com.jits.shipping.routing.Route;
import com.jits.shipping.util.ShipMethod;
import com.jits.shipping.validation.ValidateZipCode;

public class JITSPackage {
	private static final Logger LOG = LoggerFactory.getLogger(JITSPackage.class);
	
	protected ArrayList<Exception> exceptionHistory;
	private ArrayList<String> trackingHistory;
	protected String[] barcode;
	private String id;
	private ShipMethod shipMethod;
	private String fromZip;
	private String toZip;
	private double weight;
	private String other;
	private String hazards;
	private String address;
	private Route route;
	
	public JITSPackage(String barcode) throws InvalidBarcodeDataException {
		exceptionHistory = new ArrayList<>();
		trackingHistory = new ArrayList<>();
		this.barcode = BarcodeParse.disassemble(barcode);
		
		if (!validateData()){
			throw new InvalidBarcodeDataException("Invalid Barcode data", exceptionHistory);
		}
		
		setId(BarcodeParse.id(this.barcode));
		setShipMethod(BarcodeParse.shipMethod(this.barcode));
		setFromZip(BarcodeParse.fromZip(this.barcode));
		setToZip(BarcodeParse.toZip(this.barcode));				
		setWeight(BarcodeParse.weight(this.barcode));
		setOther(BarcodeParse.other(this.barcode));
		setHazards(BarcodeParse.hazards(this.barcode));
		setAddress(BarcodeParse.address(this.barcode));
		
		route = new Route(this);
		LOG.info("Package created");
	}
	
	private boolean validateData() {
		try {
			ValidateZipCode.validate(BarcodeParse.fromZip(this.barcode), "From");
		} catch (InvalidBarcodeDataException e) {
			exceptionHistory.add(e);
		}
		try {
			ValidateZipCode.validate(BarcodeParse.toZip(this.barcode), "To");
		} catch (InvalidBarcodeDataException e) {
			exceptionHistory.add(e);
		}
		
		if (exceptionHistory.size() > 0){
			return false;
		}
		else{
			return true;
		}
	}

	public String getId() {
		return id;
	}

	private void setId(String id) {
		this.id = id;
	}

	public ShipMethod getShipMethod() {
		return shipMethod;
	}

	private void setShipMethod(ShipMethod shipMethod) throws InvalidBarcodeDataException {
		this.shipMethod = shipMethod;
	}

	public String getFromZip() {
		return fromZip;
	}

	private void setFromZip(String fromZip) throws InvalidBarcodeDataException {
		this.fromZip = fromZip;
	}

	public String getToZip() {
		return toZip;
	}

	private void setToZip(String toZip) throws InvalidBarcodeDataException {
		this.toZip = toZip;
	}
	
	public double getWeight(){
		return weight;
	}

	void setWeight(double weight){
		this.weight = Math.round(weight);
	}

	public String getOther() {
		return other;
	}

	private void setOther(String other) {
		this.other = other;
	}

	public String getHazards() {
		return hazards;
	}

	private void setHazards(String hazards) {
		this.hazards = hazards;
	}
	
	public String getAddress(){
		return address;
	}
	
	private void setAddress(String address){
		this.address = address;
	}
	
	public void addToTrackingHistory(String tracking){
		trackingHistory.add(tracking);
	}
	
	public ArrayList<String> getTrackingHistory(){
		return trackingHistory;
	}
	
	public String reportInfo(){
		String format = "%-14s %3s %21s";
		String outString = String.format(format, getId(), getShipMethod(), route.routeReport());
		return outString;
	}
	
	public Route getRoute() {
		return route;
	}
}
