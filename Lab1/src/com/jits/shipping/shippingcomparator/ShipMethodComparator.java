package com.jits.shipping.shippingcomparator;

import java.util.Comparator;
import com.jits.shipping.JITSPackage;

public class ShipMethodComparator implements Comparator<JITSPackage> {

	@Override
	public int compare(JITSPackage pkg1, JITSPackage pkg2) {
		if (pkg1.getShipMethod().compareTo(pkg2.getShipMethod()) == 0){
			return pkg1.getId().compareTo(pkg2.getId());
		}
		return pkg1.getShipMethod().compareTo(pkg2.getShipMethod());
	}
}
