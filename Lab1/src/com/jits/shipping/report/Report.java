package com.jits.shipping.report;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.jits.shipping.JITSPackage;
import com.jits.shipping.shippingcomparator.ShipMethodComparator;

public class Report {
	
	public static void runReport(ArrayList<JITSPackage> packages){
		sortPackages(packages);
		
		System.out.println("JITS Shipping Package Report");
		for (JITSPackage pkg : packages){
			System.out.println(pkg.reportInfo());
		}
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println("Printed on " +dtf.format(now));
	}
	
	private static void sortPackages(ArrayList<JITSPackage> packages){
		packages.sort(new ShipMethodComparator());
	}
}
