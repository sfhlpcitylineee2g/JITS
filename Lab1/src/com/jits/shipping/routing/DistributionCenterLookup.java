package com.jits.shipping.routing;

import java.io.File;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import com.jits.shipping.util.DistributionCenter;

public class DistributionCenterLookup {
	
	public static DistributionCenter find(String zip){
		String stateAcronym = findState(zip);
		DistributionCenter dc = DistributionCenter.valueOf(stateAcronym);
		return dc;
	}
	
	private static String findState(String zipcode){
		try {
			Workbook workbook1 = Workbook.getWorkbook(new File("zip.xls"));
			Sheet sheet = workbook1.getSheet(0);
			Cell cell = sheet.findCell(zipcode);
			return sheet.getCell(cell.getColumn()+1, cell.getRow()).getContents();
		} 
		catch (Exception e) {
			return " ";
		}
	}
}
