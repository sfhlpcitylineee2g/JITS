package com.jits.shipping.routing;

import java.util.ArrayList;
import java.util.List;


import com.jits.shipping.location.Location;

public class AirRouteBuilder{
	private Location warehouseLocation;
	private Location airportOrigin;
	private Location airportDestination;
	private Location endingLocation;
	
	public AirRouteBuilder setWarehouseLocation(Location warehouseLocation){
		this.warehouseLocation = warehouseLocation;
		return this;
	}
	public AirRouteBuilder setendingLocation(Location endingLocation){
		this.endingLocation = endingLocation;
		return this;
	}
	public AirRouteBuilder setAirportOrigin(Location airportOrigin){
		this.airportOrigin = airportOrigin;
		return this;
	}
	public AirRouteBuilder setAirportDestination(Location airportDestination){
		this.airportDestination = airportDestination;
		return this;
	}
	
	public List<Location> build(){
		List<Location> route = new ArrayList<>();
		route.add(warehouseLocation);
		route.add(airportOrigin);
		route.add(airportDestination);
		route.add(endingLocation);
		return route;
	}

}
