package com.jits.shipping.routing;

import java.util.ArrayList;
import java.util.List;

import com.jits.shipping.location.Location;

public class GroundRouteBuilder{
	private Location warehouseLocation;
	private Location distributionCenterLocation;
	private Location endingLocation;
	
	public GroundRouteBuilder setWarehouseLocation(Location warehouseLocation){
		this.warehouseLocation = warehouseLocation;
		return this;
	}
	
	public GroundRouteBuilder setdistributionCenterLocation(Location distributionCenterLocation){
		this.distributionCenterLocation = distributionCenterLocation;
		return this;
	}
	
	public GroundRouteBuilder setendingLocation(Location endingLocation){
		this.endingLocation = endingLocation;
		return this;
	}
	
	public List<Location> build(){
		List<Location> route = new ArrayList<>();
		route.add(warehouseLocation);
		route.add(distributionCenterLocation);
		route.add(endingLocation);
		return route;
	}

}
