package com.jits.shipping.routing;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jits.shipping.JITSPackage;
import com.jits.shipping.location.Location;
import com.jits.shipping.util.ShipMethod;
import com.jits.shipping.util.TrackingWriter;

public class Route {
	
	private List<Location> route;
	private JITSPackage pkg;
	private Airport airport;
	private int increment;
	private int position;
	private int max;
	private int min;
	private TrackingWriter trackingWriter;
	private static final Logger LOG = LoggerFactory.getLogger(JITSPackage.class);
	
	public Route(JITSPackage pkg){
		this.pkg = pkg;
		setupRoute();
	}
	
	private void setupRoute(){
		route = new ArrayList<>();
		route = RouteFactory.valueOf(pkg.getShipMethod().name()).getRoute(pkg);
		trackingWriter = new TrackingWriter("tracking.txt", (true));
		airport = new Airport();
		position = 0;
		increment = 1;
		
		//remove when we add RAL
		if (pkg.getShipMethod().equals(ShipMethod.GRD) || pkg.getShipMethod().equals(ShipMethod.AIR)){
			pkg.addToTrackingHistory(route.get(position).getTracking());
			trackingWriter.write(route.get(position).getTracking());
		}
		
		if (pkg.getShipMethod().equals(ShipMethod.GRD)){
			max = 2;
			min = 0;
		}
		else if(pkg.getShipMethod().equals(ShipMethod.AIR)) {
			max = 3;
		}
	}
	
	public Location move(){
		position+=increment;
		if (position == max){
			increment = -1;
		}
		else if (position == min){
			increment = 1;
		}
		
		pkg.addToTrackingHistory(route.get(position).getTracking());
		trackingWriter.write(route.get(position).getTracking());
		LOG.info("Package moved");
		return route.get(position);
	}
	
	
	public Location getCurrentLocation(){
		return route.get(position);
	}
	
	public String routeReport(){
		String routingInfo = "";
		if (pkg.getShipMethod() == ShipMethod.GRD)
		{
			String distributionCenter = DistributionCenterLookup.find(pkg.getToZip()).getDistributionCenter().name();
			routingInfo = "Whse DistCtr: "+ distributionCenter + " Dest: "+pkg.getAddress(); 
		}
		else if (pkg.getShipMethod() == ShipMethod.AIR){
			airport = AirportLocator.findClosestAirport(pkg.getToZip());
			routingInfo = "Arprt:"+airport.getCode()+" "+airport.getName(); 
		}
		else if (pkg.getShipMethod() == ShipMethod.RAL){
			return routingInfo;
		}
		return routingInfo;
	}
}
