package com.jits.shipping.routing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream.Builder;

import com.jits.shipping.JITSPackage;
import com.jits.shipping.location.AirportLocation;
import com.jits.shipping.location.DistributionCenterLocation;
import com.jits.shipping.location.EndingLocation;
import com.jits.shipping.location.Location;
import com.jits.shipping.location.WarehouseLocation;

public enum RouteFactory {
	GRD(){
		
		 public List<Location>getRoute(JITSPackage jpackage){
			GroundRouteBuilder groundRouteBuilder = new GroundRouteBuilder()
			.setWarehouseLocation(new WarehouseLocation(jpackage))
			.setdistributionCenterLocation(new DistributionCenterLocation(jpackage))
			.setendingLocation(new EndingLocation(jpackage));
			return  groundRouteBuilder.build();
		}
	},
	
	AIR(){
		
		 public List<Location>  getRoute(JITSPackage jpackage){
			AirRouteBuilder airRouteBuilder = new AirRouteBuilder()
			.setWarehouseLocation(new WarehouseLocation(jpackage))
			.setAirportOrigin(new AirportLocation(jpackage, jpackage.getFromZip())) 
			.setAirportDestination(new AirportLocation(jpackage, jpackage.getToZip())) 
			.setendingLocation(new EndingLocation(jpackage));
			return  airRouteBuilder.build();
		}
	},
	
	RAL(){

		public List<Location> getRoute(JITSPackage jpackage) {
			List<Location> ls = new ArrayList<>();
			return ls;
		}
		
	};
	public abstract  List<Location> getRoute(JITSPackage jpackage);
}
