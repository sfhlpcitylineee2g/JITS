package com.jits.shipping.location;

import com.jits.shipping.JITSPackage;

public class EndingLocation extends Location{

	
	public EndingLocation(JITSPackage jpackage) {
		super(jpackage);
		
	}
	public String getLocationInfo(){
		return "Ending Zip: "+getToZip()+" Address: "+getAddress();
	}
	
	public String getTracking(){
		String addr = getAddress();
		String[]addrString = addr.split(" ");
		String concat = addrString[1]+getToZip();
		concat = concat.replaceAll("\\s+","");
		
		char[] ascii = concat.toCharArray();
		int sum = 0;
		for(char ch:ascii){
	        sum +=(int)ch;
	    }
		String addressEncoded = String.valueOf(sum);
		return addressEncoded+ "|"+jpackage.getId() + "|" + dtf.format(now);
	}
	

}
