package com.jits.shipping.location;

import com.jits.shipping.JITSPackage;

public class WarehouseLocation extends Location{
	
	public WarehouseLocation(JITSPackage jpackage) {
		super(jpackage);
		
	}
	
	public String getLocationInfo(){
		return "At warehouse";
	}
	public String getTracking(){
		return "Whse"+ "|"+jpackage.getId() + "|" + dtf.format(now);
		
	}

}
