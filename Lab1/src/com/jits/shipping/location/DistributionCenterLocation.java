package com.jits.shipping.location;

import com.jits.shipping.JITSPackage;
import com.jits.shipping.routing.DistributionCenterLookup;



public class DistributionCenterLocation extends Location{
	
	public DistributionCenterLocation(JITSPackage jpackage){
		super(jpackage);
	}
	public String getLocationInfo(){
		return "At Distribution Center: "+DistributionCenterLookup.find(getToZip()).name();
	}
	public String getTracking(){
		return DistributionCenterLookup.find(getToZip()).getDistributionCenter().name()+ "|"+jpackage.getId() + "|" + dtf.format(now);
	}
}
