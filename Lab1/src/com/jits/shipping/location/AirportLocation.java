package com.jits.shipping.location;

import com.jits.shipping.JITSPackage;
import com.jits.shipping.routing.AirportLocator;

public class AirportLocation extends Location {

	private String s;

	public AirportLocation(JITSPackage jpackage, String s) {
		super(jpackage);
		this.s =s;
		
	}
	public String getLocationInfo(){
		return "At airport";
	}
	
	public String getTracking(){		 
		return AirportLocator.findClosestAirport(s).getCode()+"|"+jpackage.getId() + "|" + dtf.format(now);
	}
	
	

}
