package com.jits.shipping.location;

import com.jits.shipping.JITSPackage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class Location {
	protected JITSPackage jpackage;
	protected DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	protected LocalDateTime now = LocalDateTime.now();
	public Location(JITSPackage jpackage) {
		this.jpackage = jpackage;
	}
	
	public String getToZip(){
		return jpackage.getToZip();
	}
	
	public String getFromZip(){
		return jpackage.getFromZip();
	}
	
	public String getAddress(){
		return jpackage.getAddress();
	}
	
	public String getLocationInfo(){
		return " ";
	}
	
	public String getTracking(){
		return jpackage.getId() + "|" + dtf.format(now);
	}
	
	public String getTracking(String zip){
		return "";
	}
	


}
