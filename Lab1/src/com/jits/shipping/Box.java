package com.jits.shipping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.parse.BarcodeParse;
import com.jits.shipping.validation.ValidateDimensions;

public class Box extends JITSPackage{

	private Dimensions dimensions;
	private static final Logger LOG = LoggerFactory.getLogger(Box.class);
	
	public Box(String barcode) throws InvalidBarcodeDataException {
		super(barcode);
		dimensions = new Dimensions();
		
		if (!validateData()){
			throw new InvalidBarcodeDataException("Invalid Barcode data", exceptionHistory);
		}
		
		setHeight(BarcodeParse.height(super.barcode));
		setWidth(BarcodeParse.width(super.barcode));
		setDepth(BarcodeParse.depth(super.barcode));
		LOG.info("Box created");
	}
	
	private boolean validateData(){	
		try {
			dimensions.setHeight(BarcodeParse.height(super.barcode));
			ValidateDimensions.validate(dimensions);
		} catch (InvalidBarcodeDataException e) {
			exceptionHistory.add(e);
		}
		try{
			dimensions.setWidth(BarcodeParse.width(super.barcode));
			ValidateDimensions.validate(dimensions);
		}
		catch (InvalidBarcodeDataException e) {
			exceptionHistory.add(e);
		}
		try{
			dimensions.setDepth(BarcodeParse.depth(super.barcode));
			ValidateDimensions.validate(dimensions);
		}
		catch (InvalidBarcodeDataException e) {
			exceptionHistory.add(e);
		}
		
		if (exceptionHistory.size() > 0){
			return false;
		}
		else{
			return true;
		}
	}

	public int getHeight() {
		return dimensions.getHeight();
	}

	private void setHeight(int height) throws InvalidBarcodeDataException {
		dimensions.setHeight(height);
	}

	public int getWidth() {
		return dimensions.getWidth();
	}

	private void setWidth(int width) throws InvalidBarcodeDataException {
		dimensions.setWidth(width);
	}

	public int getDepth() {
		return dimensions.getDepth();
	}

	private void setDepth(int depth) throws InvalidBarcodeDataException {
		dimensions.setDepth(depth);
	}
	
}
