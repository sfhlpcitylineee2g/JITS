package com.jits.shipping.parse;

import com.jits.shipping.util.BarcodeFields;
import com.jits.shipping.util.ShipMethod;

public class BarcodeParse {
		
	private BarcodeParse(){};
	
	public static String[] disassemble(String barcode){
		return barcode.split("\\|");
	}
	
	public static String id(String[] barcode){
		return barcode[BarcodeFields.ID];
	}
	
	public static ShipMethod shipMethod(String[] barcode){
		ShipMethod shipMethod = ShipMethod.valueOf(barcode[BarcodeFields.SHIPMETHOD]);
		return shipMethod;
	}
	
	public static String fromZip(String[] barcode){
		return barcode[BarcodeFields.FROMZIP];
	}
	
	public static String toZip(String[] barcode){
		return barcode[BarcodeFields.TOZIP];
	}
	
	public static double weight(String[] barcode){
		return Double.parseDouble(barcode[BarcodeFields.WEIGHT]);
	}
	
	public static int height(String[] barcode){
		return Integer.parseInt(barcode[BarcodeFields.HEIGHT]);
	}
	
	public static int width(String[] barcode){
		return Integer.parseInt(barcode[BarcodeFields.WIDTH]);
	}
	
	public static int depth(String[] barcode){
		return Integer.parseInt(barcode[BarcodeFields.DEPTH]);
	}
	
	public static String other(String[] barcode){
		return barcode[BarcodeFields.OTHER];
	}
	
	public static String hazards(String[] barcode){
		return barcode[BarcodeFields.HAZARDS];
	}
	
	public static String address(String[] barcode){
		return barcode[BarcodeFields.ADDRESS];
	}
}
