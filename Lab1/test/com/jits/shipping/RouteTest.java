package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.routing.Airport;
import com.jits.shipping.routing.AirportLocator;
import com.jits.shipping.routing.DistributionCenterLookup;


public class RouteTest {

	JITSPackage pkg;
	JITSPackage pkgAIR;
	//JITSPackage pkgRAL;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		pkg = new JITSPackage("11243545698062|GRD|75082|35042|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
		pkgAIR = new JITSPackage("11243545698062|AIR|75082|35042|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
		//pkgRAL = new JITSPackage("11243545698062|RAL|75082|75082|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
	}
	
	@Test
	public void testRoute() {
		String warehouseStringExptected = "At warehouse";
		String distributionStringExpected = "At Distribution Center: "+DistributionCenterLookup.find(pkg.getToZip());
		String endingLocationStringExpected = "Ending Zip: "+pkg.getToZip()+" Address: "+pkg.getAddress();
		assertEquals(pkg.getRoute().getCurrentLocation().getLocationInfo(), warehouseStringExptected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), distributionStringExpected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), endingLocationStringExpected);
	}
	
	@Test
	public void testMove(){
		String warehouseStringExptected = "At warehouse";
		String distributionStringExpected = "At Distribution Center: "+DistributionCenterLookup.find(pkg.getToZip());
		String endingLocationStringExpected = "Ending Zip: "+pkg.getToZip()+" Address: "+pkg.getAddress();
		assertEquals(pkg.getRoute().getCurrentLocation().getLocationInfo(), warehouseStringExptected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), distributionStringExpected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), endingLocationStringExpected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), distributionStringExpected);
		assertEquals(pkg.getRoute().move().getLocationInfo(), warehouseStringExptected);
	}
	
	@Test
	public void testAirportMove(){
		String warehouseStringExptected = "At warehouse";
		String airportStringExpected = "At airport";
		String endingLocationStringExpected = "Ending Zip: "+pkg.getToZip()+" Address: "+pkg.getAddress();
		assertEquals(pkgAIR.getRoute().getCurrentLocation().getLocationInfo(), warehouseStringExptected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), airportStringExpected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), airportStringExpected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), endingLocationStringExpected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), airportStringExpected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), airportStringExpected);
		assertEquals(pkgAIR.getRoute().move().getLocationInfo(), warehouseStringExptected);
	}
	
	@Test
	public void testRouteReport(){
		String distributionCenter = DistributionCenterLookup.find(pkg.getToZip()).getDistributionCenter().name();
		String expectedRouteReportGRD = "Whse DistCtr: "+ distributionCenter + " Dest: "+pkg.getAddress();
		
		Airport airport = AirportLocator.findClosestAirport(pkg.getToZip());
		String expectedRouteAir = "Arprt:"+airport.getCode()+" "+airport.getName();
		
		assertEquals(pkg.getRoute().routeReport(), expectedRouteReportGRD);
		assertEquals(pkgAIR.getRoute().routeReport(), expectedRouteAir);
		//assertEquals(pkgRAL.getRoute().routeReport(), expectedRouteReportOther);
	}

}
