package com.jits.shipping;


import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.location.*;
public class LocationTest {
	Location location;
	JITSPackage jpackage;
	protected DateTimeFormatter dtf;
	protected LocalDateTime now;
	
	@Before
	public void setUp() throws InvalidBarcodeDataException{
		jpackage = new JITSPackage("11243545698062|GRD|75082|75238|30.0|5|8|10|X!002YZ78W|CHM|445 B Street");
		location = new Location(jpackage);
		dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		now = LocalDateTime.now();
	}

	@Test
	public void canGetToZip() {
		assertEquals(location.getToZip(),"75238");
	}
	@Test
	public void canGetFromZip() {
		assertEquals(location.getFromZip(),"75082");
	}

	
	@Test
	public void canGetAddress() {
		assertEquals(location.getAddress(),"445 B Street");
	}
	@Test
	public void canGetLocationInfo() {
		assertEquals(location.getLocationInfo()," ");
	}
	
	@Test
	public void canGetTracking(){
		assertEquals(location.getTracking(),"11243545698062|" +dtf.format(now));
		
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testLocationFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		jpackage = new JITSPackage("11243545698062|GRD|12345A|543B21|30.0|-5|8|10|X!002YZ78W|CHM|5");
	}
}
