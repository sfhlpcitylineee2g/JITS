package com.jits.shipping;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.JITSPackage;
import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.util.ShipMethod;

public class JITSPackageTest {
	JITSPackage packages;
	Box invalidPackage;
	
	private DateTimeFormatter dtf;
	private LocalDateTime now;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		packages = new JITSPackage("11243545698062|GRD|75082|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
	
	@Test
	public void packageTest() {
		assertEquals(packages.getId(), "11243545698062");
		assertEquals(packages.getShipMethod(), ShipMethod.GRD);
		assertEquals(packages.getFromZip(), "75082");
		assertEquals(packages.getToZip(), "28772");
		assertEquals(packages.getWeight(), 30.0, 0);
		assertEquals(packages.getOther(), "X!002YZ78W");
		assertEquals(packages.getHazards(), "CHM");
		assertEquals(packages.getAddress(), "123 Main");
		assertNotNull(packages.getRoute());
	}
	
	@Test
	public void testReportInfo(){
		String format = "%-14s %3s %21s";
		String expectedString = String.format(format, packages.getId(), packages.getShipMethod(), packages.getRoute().routeReport());
		assertEquals(packages.reportInfo(), expectedString);
	}
	
	@Test
	public void testTrackingHistory(){
		dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		now = LocalDateTime.now();
		
		assertEquals(packages.getTrackingHistory().get(0), "Whse|11243545698062|"+dtf.format(now));
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testWrongShipMethod() throws IllegalArgumentException, InvalidBarcodeDataException{
		exception.expect(IllegalArgumentException.class);
		invalidPackage = new Box("11243545698062|QWE|75082|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
	
	@Test
	public void testWrongFromZip() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		invalidPackage = new Box("11243545698062|GRD|abc123|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
	
	@Test
	public void testWrongToZip() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		invalidPackage = new Box("11243545698062|GRD|75082|abc123|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
}
