package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.location.Location;

public class AirportLocationTest {

	Location location;
	JITSPackage jpackage;
	@Before
	public void setUp() throws InvalidBarcodeDataException {
		jpackage = new JITSPackage("11243545698062|GRD|75238|75204|30.0|5|8|10|X!002YZ78W|CHM|445 State Street");
		location = new Location(jpackage);
	}

	@Test
	public void canGetLocationInfo() {
		assertEquals(location.getLocationInfo(),"At airport");
	}
	public void canGetTrackingInfo(){
		assertEquals(location.getTracking(),"11243545698062");
	}
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testLocationFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		jpackage = new JITSPackage("11243545698062|GRD|12345A|543B21|30.0|-5|8|10|X!002YZ78W|CHM|5");
	}

}
