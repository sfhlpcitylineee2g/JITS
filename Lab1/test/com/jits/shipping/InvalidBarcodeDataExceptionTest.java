package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Test;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;

public class InvalidBarcodeDataExceptionTest {

	@Test
	public void test() {
		try{
			throw new InvalidBarcodeDataException();
		}
		catch(InvalidBarcodeDataException e){
			assertNotNull(e);
		}
	}

}
