package com.jits.shipping;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.report.Report;

public class ReportTest {
	
	ArrayList<JITSPackage> packages;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		packages = new ArrayList<>();
		packages.add(new Box("99912095916062|RAL|35031|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new JITSPackage("22215345916062|GRD|28772|28772|12.0|0|0|0|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new JITSPackage("55512095916062|AIR|28772|28772|3.0|0|0|0|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new JITSPackage("11142095916062|GRD|28772|28772|5.0|0|0|0|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new JITSPackage("33312095916062|GRD|28772|28772|7.0|0|0|0|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new Box("88812095916062|RAL|35031|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new Box("77712095916062|RAL|35031|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new JITSPackage("44412095916062|AIR|28772|75082|1.0|0|0|0|X!002YZ78W|CHM|123 Main Street"));
		packages.add(new Box("66612095916062|AIR|35031|28772|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street"));
	}
	
	@Test
	public void testReport() {
		Report r = new Report();
		Report.runReport(packages);
		assertEquals(packages.get(0).getId(), "11142095916062");
		assertEquals(packages.get(1).getId(), "22215345916062");
		assertEquals(packages.get(2).getId(), "33312095916062");
		assertEquals(packages.get(3).getId(), "44412095916062");
		assertEquals(packages.get(4).getId(), "55512095916062");
		assertEquals(packages.get(5).getId(), "66612095916062");
		assertEquals(packages.get(6).getId(), "77712095916062");
		assertEquals(packages.get(7).getId(), "88812095916062");
		assertEquals(packages.get(8).getId(), "99912095916062");
	}

}
