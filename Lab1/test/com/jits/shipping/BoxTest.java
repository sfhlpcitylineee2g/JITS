package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import com.jits.shipping.Box;
import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.util.ShipMethod;

public class BoxTest {
	Box box;
	Box boxFailure;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		box = new Box("11243545698062|GRD|75082|75082|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
	}
	
	@Test
	public void boxTest() {
		assertEquals(box.getId(), "11243545698062");
		assertEquals(box.getShipMethod(), ShipMethod.GRD);
		assertEquals(box.getFromZip(), "75082");
		assertEquals(box.getToZip(), "75082");
		assertEquals(box.getWeight(), 30.0, 0);
		assertEquals(box.getHeight(), 5);
		assertEquals(box.getWidth(), 8);
		assertEquals(box.getDepth(), 10);
		assertEquals(box.getOther(), "X!002YZ78W");
		assertEquals(box.getHazards(), "CHM");
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testBoxCreationFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		boxFailure = new Box("11243545698062|GRD|75082|75082|30.0|-5|8|10|X!002YZ78W|CHM|123 Main Street");
	}
}
