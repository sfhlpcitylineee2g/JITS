package com.jits.shipping;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.validation.ValidateZipCode;

public class ValidateZipCodeTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testValidateFromZipcode() throws InvalidBarcodeDataException {
		ValidateZipCode vzc = new ValidateZipCode();
		exception.expect(InvalidBarcodeDataException.class);
		exception.expectMessage("From Zipcode falls outside of accepted range");
		ValidateZipCode.validate("123abc", "From");
	}
	
	@Test
	public void testValidateToZipcode() throws InvalidBarcodeDataException {
		exception.expect(InvalidBarcodeDataException.class);
		exception.expectMessage("To Zipcode falls outside of accepted range");
		ValidateZipCode.validate("123abc", "To");
	}

}
