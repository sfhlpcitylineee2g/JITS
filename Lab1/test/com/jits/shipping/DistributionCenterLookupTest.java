package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import com.jits.shipping.routing.DistributionCenterLookup;
import com.jits.shipping.util.DistributionCenter;


public class DistributionCenterLookupTest {

	@Test
	public void testLookup() {
		DistributionCenterLookup dcl = new DistributionCenterLookup();
		assertEquals(DistributionCenterLookup.find("75082"), DistributionCenter.TX);
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	@Test
	public void testErr(){
		exception.expect(Exception.class);
		DistributionCenterLookup dcl = new DistributionCenterLookup();
		assertEquals(DistributionCenterLookup.find("aslkdjfas"), DistributionCenter.TX);
	}

}
