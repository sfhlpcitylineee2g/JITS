package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.routing.RouteFactory;

public class RouteFactoryTest {

	RouteFactory r;
	JITSPackage jpackage;
	@Before
	public void setUp() throws InvalidBarcodeDataException {
		r = RouteFactory.AIR;
		jpackage = new JITSPackage("11243545698062|GRD|75238|75204|30.0|5|8|10|X!002YZ78W|CHM|445 State Street");
	}

	@Test
	public void canGetRoute() {
		assertEquals(r.getRoute(jpackage).get(0).getLocationInfo(),"At warehouse");
		assertEquals(r.getRoute(jpackage).get(1).getLocationInfo(),"At airport");
		assertEquals(r.getRoute(jpackage).get(2).getLocationInfo(),"At airport");
		assertEquals(r.getRoute(jpackage).get(3).getLocationInfo(),"Ending Zip: 75204 Address: 445 State Street");
	//TO-DO
	}
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testRouteFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		jpackage = new JITSPackage("11243545698062|GRD|12345A|543B21|30.0|-5|8|10|X!002YZ78W|CHM|5");
	}

}
