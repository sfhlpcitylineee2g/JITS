package com.jits.shipping;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.jits.shipping.location.Location;
import com.jits.shipping.routing.GroundRouteBuilder;

public class GroundRouteBuilderTest {

	Location warehouseLocation;
	Location distributionCenterLocation;
	Location endingLocation;
	JITSPackage jpackage;
	GroundRouteBuilder g;
	List<Location> route;
	@Before
	public void setUp() throws Exception {
		route = new ArrayList<>();
		route.add(warehouseLocation);
		route.add(distributionCenterLocation);
		route.add(endingLocation);
	}
	@Test
	public void canGetWarehouseLocation() {
		assertEquals(g.setWarehouseLocation(warehouseLocation),warehouseLocation);
	}
	@Test
	public void canGetEndingLocation(){
		assertEquals(g.setendingLocation(endingLocation),endingLocation);
	}
	@Test
	public void canGetDistributionCenterLocation(){
		assertEquals(g.setdistributionCenterLocation(distributionCenterLocation),distributionCenterLocation);
	}
	public void canBuidl(){
		assertEquals(g.build(),route);
	}
}
