package com.jits.shipping;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.location.Location;
import com.jits.shipping.routing.DistributionCenterLookup;

public class DistributionCenterLocationTest {

	Location location;
	JITSPackage jpackage;
	protected DateTimeFormatter dtf;
	protected LocalDateTime now;
	@Before
	public void setUp() throws InvalidBarcodeDataException{
		jpackage = new JITSPackage("11243545698062|GRD|75238|75204|30.0|5|8|10|X!002YZ78W|CHM|445 State Street");
		location = new Location(jpackage);
		dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		now = LocalDateTime.now();
	}

	@Test
	public void canGetToZip() {
		assertEquals(location.getToZip(),"75204");
	}
	@Test
	public void canGetFromZip() {
		assertEquals(location.getFromZip(),"75238");
	}

	@Test
	public void canGetAddress() {
		assertEquals(location.getAddress(),"445 State Street");
	}
	
	@Test
	public void canGetLocationInfo() {
		jpackage.getRoute().move();
		assertEquals(jpackage.getRoute().getCurrentLocation().getLocationInfo(),"At Distribution Center: "+DistributionCenterLookup.find(jpackage.getToZip()));
	}
	public void canGetTracking(){
		assertEquals(location.getTracking(),"DC3|11243545698062|"+dtf.format(now));
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testLocationFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		jpackage = new JITSPackage("11243545698062|GRD|12345A|543B21|30.0|-5|8|10|X!002YZ78W|CHM|5");
	}

}
