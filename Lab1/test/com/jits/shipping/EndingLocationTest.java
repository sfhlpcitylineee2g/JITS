package com.jits.shipping;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.location.Location;

public class EndingLocationTest {

	Location location;
	JITSPackage jpackage;
	protected DateTimeFormatter dtf;
	protected LocalDateTime now;
	@Before
	public void setUp()throws InvalidBarcodeDataException{
		jpackage = new JITSPackage("11243545698062|GRD|75204|75238|30.0|5|8|10|X!002YZ78W|CHM|445 B Street");
		location = new Location(jpackage);
		dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		now = LocalDateTime.now();
	}

	@Test
	public void canGetToZip() {
		assertEquals(location.getToZip(),"75238");
	}
	@Test
	public void canGetFromZip() {
		assertEquals(location.getFromZip(),"75204");
	}

	@Test
	public void canGetAddress() {
		assertEquals(location.getAddress(),"445 B Street");
	}
	
	@Test
	public void canGetLocationInfo() {
		jpackage.getRoute().move();
		jpackage.getRoute().move();
		assertEquals(jpackage.getRoute().getCurrentLocation().getLocationInfo(),"Ending Zip: " +location.getToZip()+" Address: "+jpackage.getAddress());
	}
	
	@Test
	public void canGetTracking(){
		jpackage.getRoute().move();
		jpackage.getRoute().move();
		assertEquals(jpackage.getRoute().getCurrentLocation().getTracking(),"331|11243545698062|"+dtf.format(now));
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testLocationFailure() throws InvalidBarcodeDataException{
		exception.expect(InvalidBarcodeDataException.class);
		jpackage = new JITSPackage("11243545698062|GRD|123A45A|543B21|30.0|-5|8|10|X!002YZ7123W|CWM|500");
	}
	
}
