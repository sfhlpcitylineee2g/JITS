package com.jits.shipping;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import com.jits.shipping.location.Location;
import com.jits.shipping.routing.AirRouteBuilder;

public class AirRouteBuilderTest {
	AirRouteBuilder a;
	Location warehouseLocation;
	Location airportOrigin;
	Location airportDestination;
	Location endingLocation;
	JITSPackage jpackage;
	List<Location> route;
	
	@Before
	public void setUp() throws Exception {
		route = new ArrayList<>();
		route.add(warehouseLocation);
		route.add(airportOrigin);
		route.add(airportDestination);
		route.add(endingLocation);
	}

	@Test
	public void canGetWarehouseLocation() {
		assertEquals(a.setWarehouseLocation(warehouseLocation),warehouseLocation);
	}
	@Test
	public void canGetEndingLocation(){
		assertEquals(a.setendingLocation(endingLocation),endingLocation);
	}

	@Test
	public void canGetAirportOrigin() {
		assertEquals(a.setAirportOrigin(airportOrigin),airportOrigin);
	}
	@Test
	public void canGetAirportEnding(){
		assertEquals(a.setAirportDestination(airportDestination),airportDestination);
	}
	@Test
	public void canBuild(){
		assertEquals(a.build(),route);
	}
}
