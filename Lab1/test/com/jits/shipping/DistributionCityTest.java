package com.jits.shipping;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.jits.shipping.util.DistributionCity;

public class DistributionCityTest {

	@Test
	public void test() {
		assertEquals(DistributionCity.valueOf("DC2"), DistributionCity.DC2);
		
		assertEquals(DistributionCity.DC2.getCity(), "Kansas City");
	}

}
