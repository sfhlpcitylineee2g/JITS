package com.jits.shipping;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.Box;
import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.parse.BarcodeParse;
import com.jits.shipping.util.ShipMethod;

public class BarcodeParseTest {
	Box box;
	Box box2;
	Box box3;
	Box box4;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		box = new Box("11243545698062|GRD|75082|35035|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
		box2 = new Box("11243545698062|AIR|75082|35035|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
		box3 = new Box("11243545698062|RAL|75082|35035|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
	
	@Test
	public void generalParseTest() {
		assertEquals(box.getId(), "11243545698062");
		assertEquals(box.getShipMethod(), ShipMethod.GRD);
		assertEquals(box.getFromZip(), "75082");
		assertEquals(box.getToZip(), "35035");
		assertEquals(box.getWeight(), 30.0, 0);
		assertEquals(box.getHeight(), 5);
		assertEquals(box.getWidth(), 8);
		assertEquals(box.getDepth(), 10);
		assertEquals(box.getOther(), "X!002YZ78W");
		assertEquals(box.getHazards(), "CHM");
	}
	
	@Test
	public void shipMethodSwitchTest(){
		assertEquals(box2.getShipMethod(), ShipMethod.AIR);
		assertEquals(box3.getShipMethod(), ShipMethod.RAL);
	}
	
	@Test
	public void testPrivateConstructor() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<BarcodeParse> constructor = BarcodeParse.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	  constructor.setAccessible(true);
	  constructor.newInstance();
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	@Test
	public void testWrongShipMethod() throws IllegalArgumentException, InvalidBarcodeDataException {
		exception.expect(IllegalArgumentException.class);
		box4 = new Box("11243545698062|QWE|75082|35035|30.0|5|8|10|X!002YZ78W|CHM|123 Main Street");
	}
}
