package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Test;

import com.jits.shipping.util.DistributionCenter;
import com.jits.shipping.util.DistributionCity;


public class DistributionCenterTest {

	@Test
	public void testDistributionCenter() {
		assertEquals(DistributionCenter.valueOf("AL"), DistributionCenter.AL);
		assertEquals(DistributionCenter.valueOf("CA"), DistributionCenter.CA);
		assertEquals(DistributionCenter.valueOf("HI"), DistributionCenter.HI);
		assertEquals(DistributionCenter.valueOf("NH"), DistributionCenter.NH);
		assertEquals(DistributionCenter.valueOf("OR"), DistributionCenter.OR);
		assertEquals(DistributionCenter.valueOf("TX"), DistributionCenter.TX);
		
		assertEquals(DistributionCenter.AL.getDistributionCenter(), DistributionCity.DC2);
		assertEquals(DistributionCenter.CA.getDistributionCenter().name(), "DC3");
		assertEquals(DistributionCenter.HI.getDistributionCenter().name(), "NA");
		assertEquals(DistributionCenter.NH.getDistributionCenter().name(), "DC1");
		assertEquals(DistributionCenter.OR.getDistributionCenter().name(), "DC3");
		assertEquals(DistributionCenter.TX.getDistributionCenter().name(), "DC3");
	}

}
