package com.jits.shipping;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.validation.ValidateDimensions;

public class ValidateDimensionsTest {

	Dimensions dimensions;
	
	@Before
	public void setup(){
		dimensions = new Dimensions();
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testValidateHeight() throws InvalidBarcodeDataException {
		ValidateDimensions vd = new ValidateDimensions();
		exception.expect(InvalidBarcodeDataException.class);
		exception.expectMessage("Height fall outside of accepted range");
		dimensions.setHeight(-100);
		ValidateDimensions.validate(dimensions);
	}
	
	@Test
	public void testValidateWidth() throws InvalidBarcodeDataException {
		exception.expect(InvalidBarcodeDataException.class);
		exception.expectMessage("Width fall outside of accepted range");
		dimensions.setWidth(-100);
		ValidateDimensions.validate(dimensions);
	}
	
	@Test
	public void testValidateDepth() throws InvalidBarcodeDataException {
		exception.expect(InvalidBarcodeDataException.class);
		exception.expectMessage("Depth fall outside of accepted range");
		dimensions.setDepth(-100);
		ValidateDimensions.validate(dimensions);
	}

}
