package com.jits.shipping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.jits.shipping.Box;
import com.jits.shipping.exceptions.InvalidBarcodeDataException;
import com.jits.shipping.util.ShipMethod;

public class ShipMethodTest {

	Box boxGround;
	Box boxAir;
	Box boxRail;
	Box boxError;
	
	@Before
	public void setup() throws InvalidBarcodeDataException{
		boxGround = new Box("11243545698062|GRD|75082|75238|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
		boxAir = new Box("11243545698062|AIR|75082|75238|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
		boxRail = new Box("11243545698062|RAL|75082|75238|30.0|5|8|10|X!002YZ78W|CHM|123 Main");
	}
	
	@Test
	public void testShipMethod() {
		assertEquals(boxGround.getShipMethod(), ShipMethod.GRD);
		assertEquals(boxAir.getShipMethod(), ShipMethod.AIR);
		assertEquals(boxRail.getShipMethod(), ShipMethod.RAL);
		
		assertEquals(ShipMethod.valueOf("GRD"), ShipMethod.GRD);
		assertEquals(ShipMethod.values()[0], ShipMethod.GRD);
	}
	
	@Test
	public void testGetShippingType(){
		assertEquals(ShipMethod.GRD.getShippingType(), "Ground");
	}
}
